package Repository;
import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepositoryMock;
import biblioteca.repository.repoInterfaces.CartiRepositoryInterface;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

public class TestCartiRepositoryMock {

    private CartiRepositoryInterface cartiRepository;

    @Before
    public void setUp() throws Exception {
        cartiRepository = new CartiRepositoryMock();
    }

    @After
    public void tearDown() throws Exception {
        cartiRepository = null;
    }

    @Test
    public void cautaCarteDupaAutor_TC05() throws Exception {
        List<Carte> expectedResult = new ArrayList();
        for(int i = 2; i<cartiRepository.getCarti().size(); i++) {
            cartiRepository.stergeCarte(cartiRepository.getCarti().get(i));
        }
        assertArrayEquals(expectedResult.toArray(), cartiRepository.cautaCarteDupaAutor("Arghezi").toArray());
    }

    @Test
    public void cautaCarteDupaAutor_TC06() {
        List<Carte> expectedResult = new ArrayList();
        expectedResult.add(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        //expectedResult.add(Carte.getCarteFromString("Poezii;Mihail Sadoveanu;1973;Corint;poezii"));
        assertArrayEquals(expectedResult.toArray(), cartiRepository.cautaCarteDupaAutor("Mihai").toArray());
    }

    @Test
    public void cautaCarteDupaAutor_TC07() {
        List<Carte> expectedResult = new ArrayList();
        assertArrayEquals(expectedResult.toArray(), cartiRepository.cautaCarteDupaAutor("Arghezi").toArray());
    }

    @Test
    public void cautaCarteDupaAutor_TC08() {
        List<Carte> expectedResult = new ArrayList();
        expectedResult.add(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        //expectedResult.add(Carte.getCarteFromString("Poezii;Mihail Sadoveanu;1973;Corint;poezii"));
        assertArrayEquals(expectedResult.toArray(), cartiRepository.cautaCarteDupaAutor("Mihai").toArray());
    }

    @Test
    public void cautaCarteDupaAutor_TC09() {
        List<Carte> expectedResult = new ArrayList();
        expectedResult = cartiRepository.getCarti();
        //assertArrayEquals(expectedResult.toArray(), cartiRepository.cautaCarteDupaAutor("i").toArray());
    }

}

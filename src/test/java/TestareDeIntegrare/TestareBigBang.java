package TestareDeIntegrare;

import Controller.TestCartiController;
import Controller.TestCerintaIII;
import Repository.TestCartiRepositoryMock;
import biblioteca.control.CartiController;
import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepositoryMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestareBigBang {

    @Test
    public void testareUnitara_ModululA()
    {
        JUnitCore.runClasses(TestCartiController.class);
    }
    @Test
    public void testareUnitara_ModululB()
    {
        JUnitCore.runClasses(TestCartiRepositoryMock.class);
    }

    @Test
    public void testareUnitara_ModululC()
    {
        JUnitCore.runClasses(TestCerintaIII.class);
    }

    @Test
    public void testareUnitara_ModululP()
    {
        testareUnitara_ModululA();
        testareUnitara_ModululB();
        testareUnitara_ModululC();
    }
}

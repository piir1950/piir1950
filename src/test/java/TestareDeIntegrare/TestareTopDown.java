package TestareDeIntegrare;

import Controller.TestCartiController;
import Controller.TestCerintaIII;
import Repository.TestCartiRepositoryMock;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class TestareTopDown
{

    @Test
    public void testareUnitara_ModululA()
    {
        JUnitCore.runClasses(TestCartiController.class);
    }

       @Test
    public void testareDeIntegrare_ModululB()
    {
        testareUnitara_ModululA();
        JUnitCore.runClasses(TestCartiRepositoryMock.class);
    }

    @Test
    public void testareDeIntegrare_ModululC()
    {
        testareUnitara_ModululA();
        testareDeIntegrare_ModululB();
        JUnitCore.runClasses(TestCerintaIII.class);
    }






}
package Controller;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepository;
import biblioteca.repository.repoMock.CartiRepositoryMock;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TestCerintaIII {
    public CartiRepositoryMock repository= new CartiRepositoryMock();
    @Test
    public void afiseazaCarte_TC1_Valid()
    {
        List<Carte> carti=repository.getCartiOrdonateDinAnul(1948);
        assertTrue(carti.size()>0);
    }

    @Test
    public void afiseazaCarti_TC02_NonValid()
    {
        try {
            List<Carte> carti = repository.getCartiOrdonateDinAnul(2020);
        }catch (Exception exception){
            assert (exception.getMessage().equals("Nu e numar!"));
        }
    }

}

package biblioteca.control;


import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepositoryInterface;
import biblioteca.util.Validator;

import java.util.List;

public class CartiController {

	private CartiRepositoryInterface cr;
	
	public CartiController(CartiRepositoryInterface cr){
		this.cr = cr;
	}
	
	public void adaugaCarte(Carte c) throws Exception{
		Validator.validateCarte(c);
		cr.adaugaCarte(c);
	}
	
	public void modificaCarte(Carte veche, Carte noua) throws Exception{
		Validator.validateCarte(veche);
		Validator.validateCarte(noua);
		cr.modificaCarte(noua, veche);
	}
	
	public void stergeCarte(Carte c) throws Exception{
		cr.stergeCarte(c);
	}

	public List<Carte> cautaCarteDupaAutor(String autor) throws Exception{
		Validator.isStringValid(autor);
		return cr.cautaCarteDupaAutor(autor);
	}
	
	public List<Carte> getCarti() throws Exception{
		return cr.getCarti();
	}

	public List<Carte> cautaCarteDupaCuvantCheie(String cuvant) throws Exception{
		Validator.isStringOK(cuvant);
		return cr.cautaCarteDupaCuvantCheie(cuvant);
	}
	
	public List<Carte> getCartiOrdonateDinAnul(int an) throws Exception{
		if(!Validator.idValidYear(an))
			throw new Exception("Nu e numar!");
		return cr.getCartiOrdonateDinAnul(an);
	}
	
	
}

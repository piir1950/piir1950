package biblioteca.repository.repoMock;


import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepositoryInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepositoryMock implements CartiRepositoryInterface {

	private List<Carte> carti;

	public CartiRepositoryMock(){
		carti = new ArrayList<Carte>();

		carti.add(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri;Corint"));
		carti.add(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii;Corint"));
		carti.add(Carte.getCarteFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia;Pescarusul albastru"));
		carti.add(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval;Paralela 45"));
		carti.add(Carte.getCarteFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor;Corint"));
		carti.add(Carte.getCarteFromString("TestBVA;Calinescu,Tetica;1992;Pipa;am,casa;Rao"));

	}

	@Override
    public void adaugaCarte(Carte c) {
        carti.add(c);
    }

	@Override
    public List<Carte> cautaCarteDupaAutor(String autor) {
        List<Carte> carti = getCarti();
        List<Carte> cartiGasite = new ArrayList<Carte>();
        int i = 0;
        while (i < carti.size()){
            if (carti.get(i).cautaDupaAutor(autor)) {
                cartiGasite.add(carti.get(i));
            }
            i++;
        }
        return cartiGasite;
    }

	@Override
    public List<Carte> cautaCarteDupaCuvantCheie(String cuvant) {
        List<Carte> carti = getCarti();
        List<Carte> cartiGasite = new ArrayList<Carte>();
        int i=0;
        while (i<carti.size()){
            List<String> lref = carti.get(i).getCuvinteCheie();
            int j = 0;
            while(j<lref.size()){
                if(lref.get(j).toLowerCase().contains(cuvant.toLowerCase()) || lref.get(j).toLowerCase().equals(cuvant.toLowerCase())){
                    cartiGasite.add(carti.get(i));
                    break;
                }
                j++;
            }
            i++;
        }
        return cartiGasite;
    }

	@Override
    public List<Carte> getCarti() {
        return carti;
    }

	@Override
    public void modificaCarte(Carte veche, Carte noua) throws Exception {
        // TODO Auto-generated method stub
        boolean found = false;
        for(Carte c:carti) {
            if(c.equals(veche)) {
                c.setTitlu(noua.getTitlu());
                c.setAutori(noua.getAutori());
                c.setEditura(noua.getEditura());
                c.setAnAparitie(noua.getAnAparitie());
                c.setCuvinteCheie(noua.getCuvinteCheie());
                found = true;
            }
        }
        System.out.println(found);
        if(found == false) {
            System.out.println("not found!");
            throw new Exception("Nu exista cartea introdusa!");
        }
    }
    public boolean cautaCarte(Carte carte) {
        List<Carte> carti = getCarti();
        for(Carte c: carti) {
            if(c.equals(carte)) {
                return true;
            }
        }
        return false;
    }
	@Override
    public void stergeCarte(Carte c) throws Exception {
        // TODO Auto-generated method stub
        if(cautaCarte(c)==false){
            throw new Exception("Nu exista cartea!");
        }
        carti.remove(c);
    }

    @Override
    public List<Carte> getCartiOrdonateDinAnul(int an) {
        List<Carte> lc = getCarti();
        List<Carte> lca = new ArrayList<Carte>();
        for(Carte c:lc){
            if(c.getAnAparitie() == an){
                lca.add(c);
            }
        }

        Collections.sort(lca,new Comparator<Carte>(){

            @Override
            public int compare(Carte a, Carte b) {
                if(a.getTitlu().toLowerCase().compareTo(b.getTitlu().toLowerCase())==0){
                    int i=0;
                    while(i<a.getAutori().size() && i<b.getAutori().size() && a.getAutori().get(i).equals(b.getAutori().get(i))){
                        i++;
                    }
                    return a.getAutori().get(i).toLowerCase().compareTo(b.getAutori().get(i).toLowerCase());
                }
                return a.getTitlu().toLowerCase().compareTo(b.getTitlu().toLowerCase());
            }

        });

        return lca;
    }


}

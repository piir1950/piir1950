package biblioteca.repository.repoInterfaces;


import biblioteca.model.Carte;

import java.util.List;

public interface CartiRepositoryInterface {
	void adaugaCarte(Carte c);
	void modificaCarte(Carte noua, Carte veche) throws Exception;
	void stergeCarte(Carte c) throws Exception;
	List<Carte> cautaCarteDupaAutor(String autori);
	List<Carte> cautaCarteDupaCuvantCheie(String cuvant);
	List<Carte> getCarti();
	List<Carte> getCartiOrdonateDinAnul(int an);
}
